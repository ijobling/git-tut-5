This is an _output reference_ challenge, meaning that in order to complete the test, you must get the same result as exemplified in the tasks references.

{Check It!|assessment}(test-543954998)

|||guidance

### Correct answers:

Insert the third message: "v3.0 - third file edit" into the `versions.txt` file and commit the edit with the same message.

`git add -A` `git commit -m "v3.0 - third file edit"`

|||