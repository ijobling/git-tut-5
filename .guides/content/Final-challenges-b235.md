[Click here to close files opened in previous section](close_file remove/reset.txt)

This is an _output reference_ challenge, meaning that in order to complete the test, you must get the same result as exemplified in the tasks references.

**IMPORTANT**: SHA id's, usernames and dates may vary. The challenges framework checks for the commit messages and the file content insertions.

{Check It!|assessment}(test-2550862056)

|||guidance

### Correct answers:

Insert the first message: "v1.0 - first file edit" into the `versions.txt` file and commit the edit with the same message: 

`git add -A`, `git commit -m "v1.0 - first file edit"`

Then, add the "v2.0 - second file edit" message to the `versions.txt` file and commit the edit with the same message.

`git add -A`, `git commit -m "v2.0 - second file edit"`

|||