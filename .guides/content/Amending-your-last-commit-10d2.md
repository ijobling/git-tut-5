Let’s say you made a commit and then made a few minor changes. You may decide that you don’t want to add a whole new commit and instead want to add your recent changes into the last commit.

This is easily accomplished with 

```bash
git commit --amend -a --no-edit
```

or if you want to also change the message

```bash
git commit --amend -am 'updated commit and also a new message'
```

|||warning

**IMPORTANT** : only ever use the `--amend` option to update local commits. You should never amend commits that have been pushed to a public repository.

This is because just as when merging files that have the same name and location in the working directory but different content require you to fix the conflicts, Git will conflict between different commit messages of the same commit sha-id.

These conflicts may result on destroying your commit history.

|||

---
Experiment with _amending_ local commits in the next section.