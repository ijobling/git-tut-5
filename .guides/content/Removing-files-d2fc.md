|||warning

Files that are created or opened explicitly by you are not automatically closed. We would recommend you close these before proceeding.

[Click here to close these files now](close_file remove/rm.txt remove/unstage.txt)

|||

### Case 1: Force remove a file

Let's get rid of the `rm.txt` file entirely by doing a:

```bash
git rm -f remove/rm.txt
```

Executing a `git status` should still display the `unstage.txt` file as _staged_ meaning that the command only had impact on the `rm.txt` file. 

### Case 2: Unstage but not delete

_Unstaging_ means that Git will undo the latest `git add -A` so executing a `git status ` will display the `Untracked files:` message again. 

Let's unstage the `unstage.txt`  file:

```bash
git rm --cached remove/unstage.txt
```

The `git reset -- remove/unstage.txt` command has the same effect.

Execute a `git status` and see how the output gets back to the `Untracked files:` output again.