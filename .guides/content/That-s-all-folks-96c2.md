Git allows us to undo changes in order for our repos to be consistent, meaning that:

- We use commit messages that appropiately describe what is being committed
- We only commit and push file changes when necessary

In the next unit, you'll deepen your knowledge on the `git reset` command.