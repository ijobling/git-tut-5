### Remove from working directory
If you want to completely remove a file from your working directory try the following:

```bash
git rm -f file.txt
```

The `-f` option means _force_ in most Git commands. 

### Unstage but leave in working directory
Let's say you have staged a file with `git add` but then decide you do not want it staged *but* you want to retain the file in your working directory.

The following commands would do the job:

```bash
git rm --cached file.txt
```

or

```bash
git reset -- file.txt
```

---
Understand these concepts better in the next section.