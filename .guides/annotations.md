@annotation:tour intro
#A POETIC INTRODUCTION TO GIT

#Module 5 - Undoing changes in a file

Being able to change or review things that you have done at any point in time is hugely helpful. Let’s look at some simple, common use cases.

@annotation:tour undoing
#Undoing changes in a file
If you have been working away on an individual file and then decide you want to revert to the state of that file in a previous commit, then here are different ways to do it :

```
git checkout mary.txt
```
Extracts `mary.txt` from the HEAD (most recent) commit and overwrites the `mary.txt` in the working directory.

```
git checkout abc123 mary.txt [TODO - correct SHA]
```
Extracts `mary.txt` from the commit specified by the SHA value (which you get from `git log`) and overwrites `mary.txt` in the working directory.

```
git checkout HEAD~2 mary.txt
```
Extracts `mary.txt` from the 3rd most recent commit and overwrites `mary.txt` in the working directory.

@annotation:tour amend
#Amending your last commit
Let’s say you did a commit and then made a few minor changes. You may decide that you don’t want to add a whole new commit and instead want to add your recent changes into the last commit.

This is easily accomplished with 

```
git commit --amend -a -—no-edit
```

or if you want to also change the message

```
git commit --amend -am 'updated commit and also a new message'
```

**IMPORTANT** : only ever use the `--amend` option to update local commits. You should never amend commits that have been pushed to a public repository.

@annotation:tour removingfile
#Removing files from staging, tracking working directory

##Remove from Working Directory, Untrack and Unstage
If you want to completely remove a file from your working directory and unstage and stop tracking it, try the following

```
git rm mary.txt
git commit -m 'removed mary.txt'
```

##Unstage, continue tracking, leave in working directory
Let's say you have staged a file with `git add` but then decide you do not want it staged *but* you want to retain the file in your working directory and want to continue tracking it.

```
git reset -- mary.txt
```

##Unstage, stop tracking, but leave in working directory
You may want to unstage a file but leave it tracked and in your working directory

```
git rm --cached mary.txt
```
or

```
git reset HEAD mary.txt
```

@annotation:tour removingall
#Reset staging are to match most recent commit
It could happen that you want to reset your staging area to match the most recent commit while still leaving your working directory untouched. 

This would give you the opportunity to rebuild the staged snapshot from scratch.

```
git reset
```


@annotation:tour play
#Play around
It is important to reinforce what you have read by doing it. Open up some files in this project and then play around with the commands covered in this module until you feel completely comfortable.


