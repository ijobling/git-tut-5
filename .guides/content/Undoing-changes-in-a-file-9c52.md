We can say that the `undo.txt` file we edited in the previous section has 3 versions, made in 3 different commits.

### Case 1
Revert the `undo.txt` file to "Version 2.0 - second commit" using `HEAD~1`:

```bash
git checkout HEAD~1 undo.txt
```

Open the `undo.txt` file and confirm that the changes were made.

### Case 2
Revert the `undo.txt` file to "Version 1.0 - first commit" using the SHA id (use the SHA id that your `git log -3` returns):

```bash
git checkout <your sha-id> undo.txt
```

Open the `undo.txt` file and confirm that the changes were made.

### Case 3
Bring back the 3 version lines of the `undo.txt` file:

```bash
git checkout HEAD undo.txt
```