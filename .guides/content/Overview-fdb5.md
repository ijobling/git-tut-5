## Undoing changes in a file

Being able to change or review things that you have done at any point in time is hugely helpful. Let’s look at some simple, common use cases.