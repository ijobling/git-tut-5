Using the `undo.txt` file we worked with in the previous section, amend the latest commit: "Version 3.0 - third commit". 

Change the "Version 3.0 - third commit" line in the `undo.txt` file to read: "Version 3.0 - third commit (amend)". 

Git will register this as a file change and running a `git status` should output a `Changes not staged for commit` message.

Instead of staging and committing the changes as we would normally do, let's execute the

```bash
git commit --amend -a --no-edit
```

command this time and run a `git status` again:

```bash
# On branch master
# Your branch is ahead of 'origin/master' by 3 commits.
#   (use "git push" to publish your local commits) 
# 
nothing to commit, working directory clean
```

Note: The `Your branch is ahead of 'origin/master' by 3 commits` line refers to the remote repo (origin/master), in this case, doing a `git push origin master` should clean this line.

The latest commit `git log -1` should still output 

```bash
Version 3.0 - third commit
```

as the commit message.

---
Now let's perform something similar as in this section, but instead, update the commit message without creating an additional commit.