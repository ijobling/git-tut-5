If you have been working away on an individual file and then decide you want to revert to the state of that file in a previous commit, then here are different ways to do it :

```bash
git checkout file.txt
```
Extracts `file.txt` from the HEAD (most recent) commit and overwrites the `file.txt` in the working directory.

```bash
git checkout 2e35f847 file.txt
```
Extracts `file.txt` from the commit specified by the SHA value (which you get from `git log`) and overwrites `file.txt` in the working directory.

```bash
git checkout HEAD~2 file.txt
```
Extracts `file.txt` from the 3rd most recent commit and overwrites `file.txt` in the working directory.

---
Let's play around with this concept in the next section.