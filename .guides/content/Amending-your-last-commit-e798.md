If you followed along with the previous sections, the `undo.txt` file should be looking like this: 

```bash
Version 1.0 - first commit
Version 2.0 - second commit
Version 3.0 - third commit (amend)
```

And the latest commit message is: 
```bash
Version 3.0 - third commit
```

Let's make some changes to the `undo.txt` file as well as changing the commit message without creating an additional commit. 

Change the "Version 3.0 - third commit (amend)" line of the `undo.txt` file to read: "v3.0 commit message update"

Now, execute a 

```bash
git commit --amend -am "v3.0 commit message update"
```

to add the changes to the latest commit and update the message.

Executing a `git log -1` should output the new message: `"v3.0 commit message update"` and executing a `git status` should output a `nothing to commit, working directory clean` as well.