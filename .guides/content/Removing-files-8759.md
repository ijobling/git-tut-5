You are currently seeing the `remove` folder in the file tree.

First of all, make sure that running a `git status` returns a 

```bash
# On branch master
nothing to commit, working directory clean
# Your branch is ahead of 'origin/master' by 3 commits.
#   (use "git push" to publish your local commits) 
```

message. If this is not the case, just perform a normal `git commit -am "any message"` and keep going.

Create 2 files inside the `remove` folder: `rm.txt` and `unstage.txt`

### First state: Untracked files
Files that have been recognized by Git but haven't been _staged_.

Executing a `git status` again should output something like this:

```bash
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	remove/
```

### Second state: Staged files

Now, let's _stage_ both files executing a `git add -A` so a `git status` outputs this: 

```bash
On branch master
Your branch is ahead of 'origin/master' by 3 commits.
  (use "git push" to publish your local commits) 
 
Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   remove/rm.txt
	new file:   remove/unstage.txt
```

Continue in the next section.