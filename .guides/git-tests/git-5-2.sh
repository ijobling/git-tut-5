#!/bin/bash
QCOUNT=7

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Create a third version of the 'versions.txt' file" "v1.0[[:space:]]*-[[:space:]]*first[[:space:]]*file[[:space:]]*edit" "git log -3"
				;;
			2 )
				expect "Create a third version of the 'versions.txt' file" "v2.0[[:space:]]*-[[:space:]]*second[[:space:]]*file[[:space:]]*edit" "git log -3"
				;;
			3 )
				expect "Create a third version of the 'versions.txt' file" "v3.0[[:space:]]*-[[:space:]]*third[[:space:]]*file[[:space:]]*edit" "git log -3"
				;;
      4 )
				expect "Create a third version of the 'versions.txt' file" "v1.0[[:space:]]*-[[:space:]]*first[[:space:]]*file[[:space:]]*edit" "git diff HEAD~1"
				;;
			5 )
				expect "Create a third version of the 'versions.txt' file" "-v2.0[[:space:]]*-[[:space:]]*second[[:space:]]*file[[:space:]]*edit" "git diff HEAD~1"
				;;
			6 )
				expect "Create a third version of the 'versions.txt' file" "+v2.0[[:space:]]*-[[:space:]]*second[[:space:]]*file[[:space:]]*edit" "git diff HEAD~1"
				;;
			7 )
				expect "Create a third version of the 'versions.txt' file" "+v3.0[[:space:]]*-[[:space:]]*third[[:space:]]*file[[:space:]]*edit" "git diff HEAD~1"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command