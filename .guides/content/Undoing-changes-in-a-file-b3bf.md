An `undo.txt` file is available in your file tree. 

Click the file tab in the left pane and add the following text: "Version 1.0 - first commit"

Commit the file using your terminal window and add the message: "Version 1.0 - first commit"

Doing a `git log -1` (that will return the most recent commit) should output something similar to this:

```bash
commit 1234abcab0fa1d1e25505269d3c44a7bffb20df5
Author: username <user@email.com> 
Date:   Thu Nov 5 10:57:41 2015 +0000 
                                  
    Version 1.0 - first commit 
```

Let's _record_ two more changes in our `undo.txt` file in two more different commits. 

Repeat the same process as before:

- Open the `undo.txt` file and add the message: "Version 2.0 - second commit" and "Version 3.0 - third commit" committing each change so the result of executing `git log -3` (the 3 most recent commits) displays something similar to this: 

```bash
commit 1234efgd9b5940b2df36cb2f116d4d457c756736
Author: username <user@email.com>
Date:   Thu Nov 5 11:05:34 2015 +0000
                                    
    Version 3.0 - third commit 
                               
commit 1234bcdc59b74815a1da782bbdd9162180741d82f
Author: username <user@email.com>
Date:   Thu Nov 5 11:05:07 2015 +0000 
                                
    Version 2.0 - second commit 
                               
commit 1234abcab0fa1d1e25505269d3c44a7bffb20df5
Author: username <user@email.com>
Date:   Thu Nov 5 10:57:41 2015 +0000
                             
    Version 1.0 - first commit 
```

And the `undo.txt` file content looks like this (each line should have been made in a different commit):

```
Version 1.0 - first commit
Version 2.0 - second commit
Version 3.0 - third commit
```

Continue in the next section.