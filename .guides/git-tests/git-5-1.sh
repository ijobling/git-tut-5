#!/bin/bash
QCOUNT=5

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Create 2 versions of the same file to match the output reference" "v1.0[[:space:]]*-[[:space:]]*first[[:space:]]*file[[:space:]]*edit" "git log -2"
				;;
			2 )
				expect "Create 2 versions of the same file to match the output reference" "v2.0[[:space:]]*-[[:space:]]*second[[:space:]]*file[[:space:]]*edit" "git log -2"
				;;
      3 )
				expect "Create 2 versions of the same file to match the output reference" "-v1.0[[:space:]]*-[[:space:]]*first[[:space:]]*file[[:space:]]*edit" "git diff HEAD~1"
				;;
      4 )
				expect "Create 2 versions of the same file to match the output reference" "+v1.0[[:space:]]*-[[:space:]]*first[[:space:]]*file[[:space:]]*edit" "git diff HEAD~1"
				;;
      5 )
				expect "Create 2 versions of the same file to match the output reference" "+v2.0[[:space:]]*-[[:space:]]*second[[:space:]]*file[[:space:]]*edit" "git diff HEAD~1"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command