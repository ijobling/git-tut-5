Until now, we've been _unstaging_ files individually.

It could happen that you want to reset your staging area to match the most recent commit while still leaving your working directory untouched. 

In other words, _unstage_ all the file modifications or additions with one single command.

Add a `reset.txt` file to the `remove` directory and do a `git status` to see the `Untracked files:` message.

Execute a `git add -A` to stage all of the files so a `git status` returns a `Changes to be committed` message with the files that are now in the cache.

The following commands will _unstage_ all the files (try any of them):

```bash
git reset
```

or 

```bash
git rm --cached -r ./
```

The last one has an `-r` option that means _recursive_ or _execute the command for all the files inside the specified directory_ which in this case is the working directory, represented as `./`

Doing a `git status` will output a list of all the `Untracked files` that exist in the current working directory. 