In the previous unit, you became pretty comfortable with the output of `git log`. 

In the `git log` output, it is possible to recognize: 

- The SHA id of the most recent commits
- The user who created the commit
- The date of the commit
- If the commit was a merge
- The commit message

On the other hand, you played around with `git diff` which makes use of the `HEAD` concept and the commits SHA id’s that allows you to see the differences between file versions.

This is why Git is sometimes called a version control system.